# RubyPitaya

RubyPitaya is an application to create servers using the pitaya protocol.

## Installation

Run the following command:

    $ gem install rubypitaya

## Usage

    $ rubypitaya [COMMAND]

    COMMANDS:
     run            $ rubypitaya run
                    - Run server

     create         $ rubypitaya create [project_name]
                    - Create project

## Development

To create a new project using docker run the following command:

    $ docker run --rm -it -v "$PWD":/app -w /app ruby:3.2.3 bash -c "gem install rubypitaya; rubypitaya create [project_name]"

After create the new project run the following commands inside project folder to prepare your environment

    $ make build
    $ make db-create
    $ make db-migrate

Run the following command to start server:

    $ make run

Then call the following route:

    rubypitaya.helloWorldHandler.sayHello

Run command `$ make` and see the options

## Contributing

Bug reports and pull requests are welcome on Gitlab at
https://gitlab.com/LucianoPC/ruby-pitaya.
