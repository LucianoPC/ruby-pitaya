FROM ruby:3.2.3-slim

ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8

RUN apt update && \
    apt install -y --no-install-recommends \
    git \
    netcat-traditional \
    libpq-dev \
    build-essential \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app/rubypitaya/

COPY lib/rubypitaya/version.rb ./lib/rubypitaya/
COPY Gemfile Gemfile.lock rubypitaya.gemspec ./
COPY bin/setup ./bin/setup

RUN chmod u+x ./bin/setup

# RUN bundle config set path './vendor/bundle' && \
RUN ./bin/setup

COPY . .

RUN rm lib/rubypitaya/app-template/Gemfile
RUN rm lib/rubypitaya/app-template/Gemfile.lock

WORKDIR /app/rubypitaya/lib/rubypitaya/app-template/

ENTRYPOINT ["./docker/entrypoint.sh"]

CMD ["bundle", "exec", "../../../bin/rubypitaya", "run"]
