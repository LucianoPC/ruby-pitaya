#!/usr/bin/env ruby

require 'bundler/setup'
require 'active_record'

require 'rubypitaya'
require 'rubypitaya/core/database_config'

# Database connection
database_config = RubyPitaya::DatabaseConfig.new(RubyPitaya::Setup.new)
ActiveRecord::Base.establish_connection(database_config.connection_data)
ActiveRecord::Base.logger = ActiveSupport::Logger.new(STDOUT)
ActiveSupport::LogSubscriber.colorize_logging = true

# Loading core files
gem_files = Dir.glob('/app/rubypitaya/lib/rubypitaya/core/**/*.rb')
gem_files.select! {|file| !file[/.+spec.rb/] && !file[/.*(?:app\/migrations|core\/templates|core\/spec-helpers|app-template).*/]}
gem_files.each {|file| require file}

# Loading application files
gem_files = []
app_folder_paths = RubyPitaya::Path::Plugins::APP_FOLDER_PATHS + [RubyPitaya::Path::APP_FOLDER_PATH]
app_folder_paths.each do |app_folder_path|
  gem_files += Dir.glob("#{app_folder_path}**/*.rb")
end
gem_files.select! {|file| !file[/.+(?:spec.rb|spec_helper.rb)/] && !file[/.*app\/migrations.*/]}
gem_files.each {|file| require file}

# Starting irb
require 'irb'
IRB.start(__FILE__)

# Closing database connection
ActiveRecord::Base.connection.close
