require 'rubypitaya/core/path'

module RubyPitaya

  class ApplicationFilesImporter

    def import(is_cheats_enabled)
      app_folder_paths = Path::Plugins::APP_FOLDER_PATHS + [Path::Core::APP_FOLDER_PATH, Path::APP_FOLDER_PATH]

      gem_files = []

      app_folder_paths.each do |app_folder_path|
        gem_files += Dir.glob("#{app_folder_path}**/*.rb")
      end

      gem_files.select! { |a| !a[/.+app\/migrations.+/] }
      gem_files.select! { |a| !a[/.+_cheats.rb/] && !a[/.+_cheat.rb/] } unless is_cheats_enabled

      gem_files.each do |path|
        require path
      end
    end

    def auto_reload
      require 'listen'

      app_folder_paths = [Path::Core::APP_FOLDER_PATH, Path::APP_FOLDER_PATH]

      app_files_listener = Listen.to(*app_folder_paths,
                                      only: /\.rb$/,
                                      force_polling: true,
                                      latency: 0.25,
                                      wait_for_delay: 0.1) do |modified, added, removed|
        import_added_files(added)
        reload_modified_files(modified)
      end

      app_files_listener.start
    end

    private

    def import_added_files(files_path)
      Gem.find_files(files_path).each do |path|
        require path

        puts "ADDED ruby file: #{path}"
      end

      rescue Exception => error
        puts "ERROR: #{error}"
        puts error.backtrace
    end

    def reload_modified_files(files_path)
      files_path.each do |path|
        load(path)

        puts "MODIFIED ruby file: #{path}"
      end

      rescue Exception => error
        puts "ERROR: #{error}"
        puts error.backtrace
    end
  end
end
