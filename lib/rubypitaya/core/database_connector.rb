require 'active_record'
require 'rubypitaya/core/database_config'

module RubyPitaya

  class DatabaseConnector

    def initialize(setup, logger = nil)
      @database_config = DatabaseConfig.new(setup)
      @logger = ActiveSupport::Logger.new(STDOUT)
      # TODO: Use this
      # @logger = logger || ActiveSupport::Logger.new(STDOUT)
    end

    def connect
      ActiveRecord::Base.establish_connection(@database_config.connection_data)
      ActiveRecord::Base.logger = @logger
      ActiveSupport::LogSubscriber.colorize_logging = true
    end

    def disconnect
      ActiveRecord::Base.connection.close
    end
  end
end