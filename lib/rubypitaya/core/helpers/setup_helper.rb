require 'active_record'

require 'rubypitaya/core/setup'
require 'rubypitaya/core/database_config'

# Database connection
database_config = RubyPitaya::DatabaseConfig.new(RubyPitaya::Setup.new)
ActiveRecord::Base.establish_connection(database_config.connection_data)

connection_data = database_config.connection_data
migrations_paths = [RubyPitaya::Path::Core::MIGRATIONS_FOLDER_PATH]
migrations_paths += RubyPitaya::Path::Plugins::MIGRATIONS_FOLDER_PATHS
migrations_paths += [RubyPitaya::Path::MIGRATIONS_FOLDER_PATH]
ActiveRecord::Migrator.migrations_paths = migrations_paths
ActiveRecord::Migration.maintain_test_schema!

# Loading core files
Gem.find_files('rubypitaya/**/*.rb').each do |path|
  require path unless path.end_with?('spec.rb') ||
                      path.include?('app/migrations') ||
                      path.include?('core/templates') ||
                      path.include?('core/spec-helpers') ||
                      path.include?('app-template')
end

# Loading application files
app_folder_paths = RubyPitaya::Path::Plugins::APP_FOLDER_PATHS + [RubyPitaya::Path::APP_FOLDER_PATH]
app_folder_paths.each do |app_folder_path|
  app_files_path = File.join(app_folder_path, '**/*.rb')

  Dir[app_files_path].each do |path|
    require path unless path.end_with?('spec.rb') ||
                        path.include?('app/migrations')
  end
end
