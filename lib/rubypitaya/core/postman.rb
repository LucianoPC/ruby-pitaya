require 'rubypitaya/core/nats_connector'

module RubyPitaya

  class Postman

    MESSAGE_ROUTE_BIND_SESSION = 'sys.bindsession'

    def initialize(nats_connector)
      @nats_connector = nats_connector
    end

    def bind_session(session)
      route = MESSAGE_ROUTE_BIND_SESSION

      nats_session = NatsSession.new(
        id: session.id,
        uid: session.uid.to_s,
        data: session.data.to_json,
      )

      payload = NatsSession.encode(nats_session)

      response = @nats_connector.push_to_frontend(session, route, payload)

      response
    end

    def push_to_user(uid, message_route, payload)
      @nats_connector.push_to_user(uid, message_route, payload)
    end
  end
end
