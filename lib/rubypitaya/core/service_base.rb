module RubyPitaya

  class ServiceBase

    def connect
      raise "Service connect method not implemented"
    end

    def disconnect
      raise "Service disconnect method not implemented"
    end

    def client
      raise "Service client method not implemented"
    end

    def clear_all_data
      environment_name = ENV.fetch('RUBYPITAYA_SERVER_ENVIRONMENT', 'development')
      is_test_environment = environment_name == 'test'

      on_clear_all_data if is_test_environment
    end

    protected

    def on_clear_all_data
      raise "Service on_clear_all_data method not implemented"
    end
  end
end
