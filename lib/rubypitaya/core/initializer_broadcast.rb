require 'rubypitaya/core/path'
require 'rubypitaya/core/initializer_base'

module RubyPitaya

  class InitializerBroadcast

    def run(initializer_content)
      app_classes = []
      plugin_classes = []

      ObjectSpace.each_object(InitializerBase.singleton_class) do |klass|
        is_plugin_class = klass.path.include?('plugins')

        if is_plugin_class
          plugin_classes << klass
        else
          app_classes << klass
        end
      end

      plugin_classes.each do |klass|
        instance = klass.new
        instance.run(initializer_content)
      end

      app_classes.each do |klass|
        instance = klass.new
        instance.run(initializer_content)
      end
    end
  end
end
