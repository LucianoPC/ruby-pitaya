module RubyPitaya

  class ServiceHolder

    def initialize
      @services = {}
    end

    def add(key, service)
      check_service_instance(service)
      @services[key] = service
    end

    def [](key)
      @services[key].client
    end

    def has?(key)
      @services.has_key?(key)
    end

    def all_services
      @services.values
    end

    private

    def check_service_instance(service)
      raise not_a_service_message(service) unless service.is_a? ServiceBase
    end

    def not_a_service_message(service)
      "Service #{service.class} is not inheriting from RubyPitaya::ServiceBase"
    end
  end
end
