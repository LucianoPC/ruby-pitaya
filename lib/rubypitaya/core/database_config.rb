require 'erb'
require 'yaml'

require 'rubypitaya/core/setup'

module RubyPitaya

  class DatabaseConfig

    def initialize(setup)
      @config = {
        'adapter' => 'postgresql',
        'encoding' => 'unicode',
        'pool' => setup.fetch('rubypitaya.database.pool', 5),
        'host' => setup['rubypitaya.database.host'],
        'port' => setup['rubypitaya.database.port'],
        'user' => setup['rubypitaya.database.user'],
        'password' => setup['rubypitaya.database.password'],
        'database' => setup['rubypitaya.database.name'],
      }

      environment_name = setup.fetch('rubypitaya.server.environment', 'development')

      @config['database'] = "#{@config['database']}_test" if environment_name == 'test'
    end

    def config
      @config
    end

    def connection_data
      {
        'adapter': config['adapter'],
        'encoding': config['encoding'],
        'pool': config['pool'],
        'host': config['host'],
        'port': config['port'],
        'user': config['user'],
        'password': config['password'],
        'database': config['database'],
      }
    end

    def connection_data_without_database
      {
        'adapter': config['adapter'],
        'encoding': config['encoding'],
        'pool': config['pool'],
        'host': config['host'],
        'port': config['port'],
        'user': config['user'],
        'password': config['password'],
      }
    end

    def database_name
      config['database']
    end
  end
end
