module RubyPitaya

  class StatusCodes
    # Success codes
    CODE_OK = 'RP-200'

    # Error codes
    CODE_ERROR = 'RP-201'
    CODE_UNKNOWN = 'RP-000'
    CODE_HANDLER_NOT_FOUND = 'RP-001'
    CODE_ACTION_NOT_FOUND = 'RP-002'
    CODE_NOT_AUTHENTICATED = 'RP-003'
    CODE_AUTHENTICATION_ERROR = 'RP-004'
    CODE_INTERNAL_ERROR = 'RP-005'

    # Error messages
    MESSAGE_INTERNAL_ERROR = 'Internal server error'

    class Connector
      CODE_UNKNOWN = 'PIT-000'
    end
  end
end