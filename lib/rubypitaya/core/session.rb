module RubyPitaya

  class Session

    attr_reader :id, :uid, :data, :metadata, :frontend_id

    attr_writer :uid, :data

    def initialize
      @id = ''
      @uid = nil
      @data = {}
      @metadata = {}
      @frontend_id = ''
    end

    def update(id, uid, data, metadata, frontend_id)
      @id = id
      @uid = uid
      @data = data
      @metadata = metadata
      @frontend_id = frontend_id
    end

    def authenticated?
      !@uid.nil?
    end

    def user_id
      @uid
    end

    def user_id=(value)
      @uid = value
    end

    def clear
      @id = ''
      @uid = nil
      @data = {}
      @metadata = {}
      @frontend_id = ''
    end
  end
end