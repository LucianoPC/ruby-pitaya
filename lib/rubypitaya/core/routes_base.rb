
module RubyPitaya

  class RoutesBase

    def initialize
      @handler_name_map = {}

      setup
    end

    def setup
    end

    def match(route_name, to:)
      handler_name, action_name = to.split('#')
      new_handler_name, new_action_name = route_name.split('.')

      if new_action_name.nil?
        set_handler_name(handler_name, new_handler_name)
      end
    end

    def get_new_handler_name(handler_name)
      return handler_name unless @handler_name_map.include?(handler_name)

      @handler_name_map[handler_name]
    end

    private

    def set_handler_name(handler_name, new_handler_name)
      @handler_name_map[handler_name] = new_handler_name
    end
  end
end