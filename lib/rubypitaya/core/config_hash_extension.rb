module RubyPitaya

  module ConfigHashExtension
    def [](key)
      super(key.to_sym)
    end

    def fetch(*args)
      args[0] = args[0].to_sym
      super(*args)
    end

    def dig(*args)
      super(*args.map(&:to_sym))
    end

    def key?(key)
      super(key.to_sym)
    end

    def has_key?(key)
      super(key.to_sym)
    end
  end
end