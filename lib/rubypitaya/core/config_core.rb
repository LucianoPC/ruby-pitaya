require 'rubypitaya/core/config_hash_extension'

module RubyPitaya

  class ConfigCore

    def initialize()
      @config = {}
      @config.extend(ConfigHashExtension)

      configs_folder_paths = Path::Plugins::APP_CONFIG_FOLDER_PATHS + [Path::APP_CONFIG_FOLDER_PATH]

      configs_folder_paths.each do |configs_folder_path|
        path_to_all_files = File.join(configs_folder_path, '**/*.json')
        config_files = Dir.glob(path_to_all_files)

        config_files.each do |config_file|
          load_config_file(configs_folder_path, config_file)
        end
      end
    end

    def [](key)
      @config[key]
    end

    def auto_reload
      require 'listen'

      configs_folder_path = Path::APP_CONFIG_FOLDER_PATH

      config_files_listener = Listen.to(configs_folder_path,
                                        only: /\.json$/,
                                        force_polling: true,
                                        latency: 0.25,
                                        wait_for_delay: 0.1) do |modified, added, removed|
        import_added_files(configs_folder_path, added)
        reload_modified_files(configs_folder_path, modified)
      end

      config_files_listener.start
    end

    private

    def load_config_file(configs_folder_path, file_path)
      config_text = File.open(file_path, &:read)
      config_hash = JSON.parse(config_text, symbolize_names: true)
      config_hash.extend(ConfigHashExtension) if config_hash.is_a?(Hash)

      file_name = file_path.sub(/^#{configs_folder_path}/, '')[0..-6]

      @config[file_name.to_sym] = config_hash

      rescue Exception => error
        puts "ERROR: #{error}"
        puts error.backtrace
    end

    def import_added_files(configs_folder_path, files_path)
      files_path.each do |path|
        load_config_file(configs_folder_path, path)

        puts "ADDED config: #{path}"
      end
    end

    def reload_modified_files(configs_folder_path, files_path)
      files_path.each do |path|
        load_config_file(configs_folder_path, path)

        puts "MODIFIED @config: #{path}"
      end
    end
  end
end