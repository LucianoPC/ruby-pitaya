module RubyPitaya

  class Path
    APP_TEMPLATE_FOLDER_PATH = File.join(__dir__, '../app-template/')
    MIGRATION_TEMPLATE_PATH = File.join(__dir__, 'templates/template_migration.rb.erb')

    APP_FOLDER_PATH = File.join(Dir.pwd, 'app/')
    HANDLERS_FOLDER_PATH = File.join(Dir.pwd, 'app/handlers/')
    APP_CONFIG_FOLDER_PATH = File.join(Dir.pwd, 'app/config/')
    APP_SETUP_FOLDER_PATH = File.join(Dir.pwd, 'app/setup/')
    MIGRATIONS_FOLDER_PATH = File.join(Dir.pwd, 'app/migrations/')
    PLUGINS_FOLDER_PATH = File.join(Dir.pwd, 'plugins/')
    HTTP_VIEWS_FOLDER_PATH = File.join(Dir.pwd, 'app/http/views/')

    ROUTES_FILE_PATH = File.join(Dir.pwd, 'config/routes.rb')
    PLUGIN_CONFIG_FILE_PATH = File.join(Dir.pwd, 'plugins.yaml')


    class Core
      APP_FOLDER_PATH = File.join(__dir__, 'app/')
      MIGRATIONS_FOLDER_PATH = File.join(__dir__, 'app/migrations/')
    end

    class Plugins
      APP_FOLDER_PATHS = Dir.glob(File.join(Dir.pwd, 'plugins/*/app/'))
      HANDLERS_FOLDER_PATHS = Dir.glob(File.join(Dir.pwd, 'plugins/*/app/handlers'))
      APP_CONFIG_FOLDER_PATHS = Dir.glob(File.join(Dir.pwd, 'plugins/*/app/config/'))
      APP_SETUP_FOLDER_PATHS = Dir.glob(File.join(Dir.pwd, 'plugins/*/app/setup/'))
      MIGRATIONS_FOLDER_PATHS = Dir.glob(File.join(Dir.pwd, 'plugins/*/app/migrations/'))
      HTTP_VIEWS_FOLDER_PATHS = Dir.glob(File.join(Dir.pwd, 'plugins/*/app/http/views/'))
    end
  end
end
