require 'rubypitaya/core/config_core'
require 'rubypitaya/core/config_hash_extension'

module RubyPitaya

  class Config

    def initialize
      @config_core = ConfigCore.new
      @config_core_override = nil
      @has_config_core_override = false

      @empty_hash = {}
      @result_cache = {}

      @empty_hash.extend(ConfigHashExtension)
      @result_cache.extend(ConfigHashExtension)
    end

    def [](key)
      result = @result_cache[key]
      return result unless result.nil?

      if @has_config_core_override
        result = @config_core_override[key]
        result = @config_core[key] if result.nil?
      else
        result = @config_core[key]
      end

      @result_cache[key] = result

      result
    end

    def fetch(*args)
      result = self[args[0]]
      return result unless result.nil?

      @empty_hash.fetch(*args)
    end

    def auto_reload
      @config_core.auto_reload
      @config_core_override.auto_reload unless @config_core_override.nil?
    end

    def clear_cache
      @result_cache.clear
    end

    def config_core_override=(value)
      @config_core_override = value
      @has_config_core_override = !value.nil?
    end
  end
end