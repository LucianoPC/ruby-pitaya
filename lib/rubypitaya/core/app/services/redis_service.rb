require 'redis'
require 'rubypitaya/core/service_base'

module RubyPitaya

  class RedisService < ServiceBase

    def initialize(setup)
      @redis = nil
      @redis_address = setup['rubypitaya.redis.url']
    end

    def connect
      @redis = Redis.new(
        url: @redis_address,
        :reconnect_attempts => [1.5]*10,
      )

      @redis.ping
    end

    def disconnect
      @redis.close
    end

    def client
      @redis
    end

    protected

    def on_clear_all_data
      @redis.flushall
    end
  end
end
