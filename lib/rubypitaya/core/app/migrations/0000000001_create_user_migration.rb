require 'active_record'

class CreateUserMigration < ActiveRecord::Migration[6.1]

  enable_extension 'uuid-ossp' unless extension_enabled?('uuid-ossp')

  def change
    create_table :users do |t|
      t.uuid :public_id, default: 'uuid_generate_v1()', null: false, index: { unique: true }
      t.timestamps
    end
  end
end
