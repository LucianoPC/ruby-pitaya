module RubyPitaya

  class ConfigSpecHelper

    def initialize(default_config = {})
      @config_mock = {}
      @default_config = default_config
    end

    def [](key)
      result = @config_mock[key]
      return result unless result.nil?

      return @default_config[key]
    end

    def auto_reload
    end

    def config_mock=(value)
      @config_mock = value.deep_symbolize_keys.stringify_keys
    end

    def config_core_override=(value)
    end

    def add(key, value)
      keys = key.split('.')
      add_hash = undig(*keys, value)
      @config_mock = merge_recursively(@config_mock, add_hash).deep_symbolize_keys.stringify_keys
    end

    def clear
      @config_mock = {}
    end

    private

    def undig(*keys, value)
      keys.empty? ? value : { keys.first=>undig(*keys.drop(1), value) }
    end

    def merge_recursively(a, b)
      a.merge(b) do |key, a_item, b_item|
        if a_item.is_a?(Hash) && b_item.is_a?(Hash)
          merge_recursively(a_item, b_item)
        else
          a[key] = b_item
        end
      end
    end
  end
end
