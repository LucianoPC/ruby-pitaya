require 'rspec'

ENV['RUBYPITAYA_ENV'] = 'test'

require 'rubypitaya/core/helpers/setup_helper'
require 'rubypitaya/core/spec-helpers/app_spec_helper'

RSpec.configure do |config|
  config.include RubyPitaya::AppSpecHelper

  config.before(:suite) do
    RubyPitaya::AppSpecHelper.initialize_before_suite
  end

  config.after(:suite) do
    RubyPitaya::AppSpecHelper.finalize_after_suite
  end

  config.before(:each) do
    RubyPitaya::AppSpecHelper.update_before_each
    ActiveRecord::Base.descendants.each { |c| c.delete_all unless c == ActiveRecord::SchemaMigration }
  end

  config.after(:each) do
    ActiveRecord::Base.connection_handler.clear_active_connections!
  end
end