module RubyPitaya

  class PostmanSpecHelper

    def initialize
      @postman_mock = nil
    end

    def bind_session(session)
      response = @postman_mock&.bind_session(session)
      response ||= {}
      response
    end

    def push_to_user(uid, message_route, payload)
      response = @postman_mock&.push_to_user(uid, message_route, payload)
      response ||= {}
      response
    end
  end
end
