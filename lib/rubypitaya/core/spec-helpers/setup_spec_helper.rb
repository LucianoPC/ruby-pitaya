module RubyPitaya

  class SetupSpecHelper

    def initialize(default_setup = {})
      @empty_hash = {}
      @setup_mock = {}
      @default_setup = default_setup
    end

    def [](key)
      split_key = key.split('.')
      result = @setup_mock.dig(*split_key)
      return result unless result.nil?

      @default_setup.dig(*split_key)
    end

    def auto_reload
    end

    def setup_mock=(value)
      @setup_mock = value
    end

    def add(key, value)
      keys = key.split('.')
      add_hash = undig(*keys, value)
      @setup_mock = merge_recursively(@setup_mock, add_hash)
    end

    def fetch(*args)
      result = self[args[0]]
      return result unless result.nil?

      @empty_hash.fetch(*args)
    end

    def clear
      @setup_mock = {}
    end

    private

    def undig(*keys, value)
      keys.empty? ? value : { keys.first=>undig(*keys.drop(1), value) }
    end

    def merge_recursively(a, b)
      a.merge(b) do |key, a_item, b_item|
        if a_item.is_a?(Hash) && b_item.is_a?(Hash)
          merge_recursively(a_item, b_item)
        else
          a[key] = b_item
        end
      end
    end
  end
end
