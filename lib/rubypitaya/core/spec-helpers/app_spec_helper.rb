require 'rubypitaya/core/setup'
require 'rubypitaya/core/handler_router'
require 'rubypitaya/core/service_holder'
require 'rubypitaya/core/spec-helpers/setup_spec_helper'
require 'rubypitaya/core/spec-helpers/config_spec_helper'
require 'rubypitaya/core/spec-helpers/postman_spec_helper'

module RubyPitaya

  module AppSpecHelper

    def self.session
      @@session
    end

    def self.initialize_before_suite
      default_setup = Setup.new.get_config
      default_config = ConfigCore.new

      @@log = Logger.new('/dev/null')
      @@setup = SetupSpecHelper.new(default_setup)
      @@config = ConfigSpecHelper.new(default_config)
      @@session = Session.new
      @@postman = PostmanSpecHelper.new
      @@services = ServiceHolder.new

      is_cheats_enabled = @@setup.fetch('rubypitaya.server.cheats', true)
      @@handler_router ||= HandlerRouter.new(is_cheats_enabled)


      @@initializer_content = InitializerContent.new(@@log,
                                                     @@services,
                                                     @@setup,
                                                     @@config,
                                                     @@handler_router.handlers)
      @@initializer_broadcast = InitializerBroadcast.new
      @@initializer_broadcast.run(@@initializer_content)

      connect_services
    end

    def self.finalize_after_suite
      clear_all_services_data
      disconnect_services
    end

    def self.update_before_each
      @@response = {}
      @@setup.clear
      @@config.clear
      @@session.clear
      clear_all_services_data
    end

    def initialize(context)
    end

    def request(route, params = {})
      handler_name, action_name = route.split('.')[1..-1]

      @@response = @@handler_router.call(handler_name, action_name, @@session,
                                         @@postman, @@services, @@setup,
                                         @@config, @@log, params)
      rescue RouteError => error
        @@response = {
          code: error.code,
          message: error.message
        }
    end

    def response
      @@response
    end

    def authenticate(user_id)
      @@session.uid = user_id
    end

    def set_config(config)
      @@config.config_mock = config
    end

    def set_setup(setup)
      @@setup.setup_mock = setup
    end

    def set_postman(postman)
      @@postman.postman_mock = postman
    end

    def add_config(*keys, value)
      @@config.add(*keys, value)
    end

    def add_setup(*keys, value)
      @@setup.add(*keys, value)
    end

    def log
      @@log
    end

    def setup
      @@setup
    end

    def config
      @@config
    end

    def session
      @@session
    end

    def postman
      @@postman
    end

    def services
      @@services
    end

    private

    def self.connect_services
      @@services.all_services.map(&:connect)
    end

    def self.disconnect_services
      @@services.all_services.map(&:disconnect)
    end

    def self.clear_all_services_data
      @@services.all_services.map(&:clear_all_data)
    end
  end
end