require 'rubypitaya/core/instance_holder'

module RubyPitaya

  class HandlerBase

    class_attribute :non_authenticated_routes, default: []
    class_attribute :handler_objects, default: nil, instance_reader: false, instance_writer: false, instance_accessor: false, instance_predicate: false

    attr_reader :objects, :log, :services, :setup, :config, :params, :session, :postman

    def initialize
      @log = nil
      @services = nil
      @setup = nil
      @config = nil
      @params = nil
      @session = nil
      @postman = nil

      @objects = self.class.objects
    end

    def set_attributes(log, services, setup, config, params, session, postman)
      @log = log
      @services = services
      @setup = setup
      @config = config
      @params = params
      @session = session
      @postman = postman
    end

    def self.objects
      self.handler_objects ||= InstanceHolder.new
      return self.handler_objects
    end

    def self.non_authenticated_actions(*action_names)
      self.non_authenticated_routes = action_names.map(&:to_s)
    end

    def self.authenticated_action_name?(action_name)
      !self.non_authenticated_routes.include?(action_name.to_s)
    end
  end
end