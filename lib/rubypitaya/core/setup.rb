require 'rubypitaya/core/path'

module RubyPitaya

  class Setup

    def initialize()
      @config = {}
      @empty_hash = {}
      configs_folder_paths = Path::Plugins::APP_SETUP_FOLDER_PATHS + [Path::APP_SETUP_FOLDER_PATH]

      configs_folder_paths.each do |configs_folder_path|
        path_to_all_files = File.join(configs_folder_path, '**/*.yml')
        config_files = Dir.glob(path_to_all_files)

        config_files.each do |config_file|
          load_config_file(configs_folder_path, config_file)
        end
      end
    end

    def [](key)
      result = get_config_from_env_var(key)
      return result unless result.nil?

      split_key = key.split('.')
      @config.dig(*split_key)
    end

    def fetch(*args)
      result = self[args[0]]
      return result unless result.nil?

      @empty_hash.fetch(*args)
    end

    def get_config
      @config.deep_dup
    end

    def auto_reload
      require 'listen'

      configs_folder_path = Path::APP_CONFIG_FOLDER_PATH

      config_files_listener = Listen.to(configs_folder_path,
                                        only: /\.yml$/,
                                        force_polling: true,
                                        latency: 0.25,
                                        wait_for_delay: 0.1) do |modified, added, removed|
        import_added_files(configs_folder_path, added)
        reload_modified_files(configs_folder_path, modified)
      end

      config_files_listener.start
    end

    private

    def get_config_from_env_var(key)
      env_key = key.gsub('.', '_').upcase
      ENV.fetch(env_key) { nil }
    end

    def load_config_file(configs_folder_path, file_path)
      config_text = File.open(file_path, &:read)
      config_hash = YAML.load(ERB.new(config_text).result)

      path_array = file_path.sub(/^#{configs_folder_path}/, '')[0..-5]
                              .split('/')

      set_config_value(path_array, config_hash)

      rescue Exception => error
        puts "ERROR: #{error}"
        puts error.backtrace
    end

    def import_added_files(configs_folder_path, files_path)
      files_path.each do |path|
        load_config_file(configs_folder_path, path)

        puts "ADDED config: #{path}"
      end
    end

    def reload_modified_files(configs_folder_path, files_path)
      files_path.each do |path|
        load_config_file(configs_folder_path, path)

        puts "MODIFIED @config: #{path}"
      end
    end

    def set_config_value(keys, value)
      config = @config

      keys.each_with_index do |key, index|
        is_last_index = index == keys.size - 1

        if is_last_index
          config[key] = value
        else
          config[key] = {} unless config.key?(key)
          config = config[key]
        end
      end
    end
  end
end