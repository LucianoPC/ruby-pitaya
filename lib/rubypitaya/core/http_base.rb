require 'rubypitaya/core/instance_holder'

module RubyPitaya

  class HttpBase

    class_attribute :handler_objects, default: nil, instance_reader: false, instance_writer: false, instance_accessor: false, instance_predicate: false

    attr_reader :objects

    def initialize
      @objects = self.class.objects
    end

    def self.objects
      self.handler_objects ||= InstanceHolder.new
      return self.handler_objects
    end
  end
end