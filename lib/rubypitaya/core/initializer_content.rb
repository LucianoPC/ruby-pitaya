module RubyPitaya

  class InitializerContent

    attr_reader :log, :services, :setup, :config, :handlers

    def initialize(log, services, setup, config, handlers)
      @log = log
      @services = services
      @setup = setup
      @config = config
      @handlers = handlers
    end
  end
end