module RubyPitaya
  class RouteError < StandardError

    attr_reader :code, :message

    def initialize(code, message)
      @code = code
      @message = message

      super(message)
    end
  end
end