module RubyPitaya

  class InstanceHolder

    def initialize
      @instances = {}
    end

    def add(key, instance)
      @instances[key] = instance
    end

    def [](key)
      @instances[key]
    end
  end
end