Feature: Hello World

  As a developer I want to see the hello world messages

  Scenario: Hello World message
    Given the client calls the route 'rubypitaya.helloWorldHandler.sayHello'
    Then  the server response 'code' should be 'RP-200'
    And   the server response 'data.message' should be 'Hello!'

  Scenario: Custom message
    Given the client calls the route 'rubypitaya.helloWorldHandler.showMessage' with params
      | message      |
      | Hello World! |
    Then  the server response 'code' should be 'RP-200'
    And   the server response 'data.message' should be 'Hello World!'

  Scenario: Custom message with json
    Given the client calls the route 'rubypitaya.helloWorldHandler.showMessage' with json
      """
      {
        "message": "Hello World!"
      }
      """
    Then  the server response should be the following json
      """
      {
        "code": "RP-200",
        "data": {
          "message": "Hello World!"
        }
      }
      """