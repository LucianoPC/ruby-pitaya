Feature: Player

  As a game player I want to have a player on database

  Scenario: Create new player
    Given the config key 'initial_player.wallet.gold' is '10'
    And   the config key 'initial_player.name' is 'Guest'
    When  the client calls the route 'rubypitaya.playerHandler.authenticate'
    Then  the server response 'code' should be 'RP-200'
    And   the server response 'data.name' should be 'Guest'
    And   the server response 'data.gold' should be '10'

  Scenario: Get player info
    Given the following Player
      | name    | gold | user_id |
      | Someone | 15   |       1 |
    And   the Player 'Someone' is authenticated
    When  the client calls the route 'rubypitaya.playerHandler.getInfo'
    Then  the server response should be the following json
      """
      {
        "code": "RP-200",
        "data": {
          "name": "Someone",
          "gold": 15,
          "userId": 1
        }
      }
      """
    And   the server response 'data' should be the following json
      """
      {
        "name": "Someone",
        "gold": 15,
        "userId": 1
      }
      """
    And   the server response should contains the following json
      """
      {
        "data": {
          "gold": 15,
          "userId": 1
        }
      }
      """
