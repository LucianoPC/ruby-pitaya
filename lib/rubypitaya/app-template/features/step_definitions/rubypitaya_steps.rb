Given(/^the User ["'](.+)["'] is authenticated$/) do |user_id|
  user = User.find(user_id)
  @app_helper.authenticate(user.id)
end

Given(/^the setup key ["'](.+)["'] is ["'](.+)["']$/) do |key, value|
  @app_helper.add_setup(key, value)
end

Given(/^the config key ["'](.+)["'] is ["'](.+)["']$/) do |key, value|
  @app_helper.add_config(key, value)
end

Given(/^the config is the following json$/) do |config_json|
  config = JSON.parse(config_json)
  @app_helper.set_config(config)
end

Given(/^(?:now\s)?today is ["'](.+)["']$/) do |date_text|
  time_now = date_text.to_time

  Time.stub(:now).and_return(time_now)
end

When(/^the client calls the route ["'](.+)["']$/) do |route|
  @app_helper.request(route)
end

When(/^the client calls the route ["'](.+)["'] with params$/) do |route, table|
  params = table.hashes.first.symbolize_keys
  @app_helper.request(route, params)
end

When(/^the client calls the route ["'](.+)["'] with json$/) do |route, json|
  params = JSON.parse(json, symbolize_names: true)
  @app_helper.request(route, params)
end

Then(/^the server response ["'](.+)["'] should be ["'](.+)["']$/) do |response_key, expected_value|
  response_value = @app_helper.response.dig(*response_key.split('.').map(&:to_sym))

  response_value = to_string(response_value)
  expected_value = to_string(expected_value)

  expect(response_value.to_s).to eq(expected_value)
end

Then(/^the server response ["'](.+)["'] should not be ["'](.+)["']$/) do |response_key, expected_value|
  response_value = @app_helper.response.dig(*response_key.split('.').map(&:to_sym))

  response_value = to_string(response_value)
  expected_value = to_string(expected_value)

  expect(response_value.to_s).not_to eq(expected_value)
end

Then(/^the server response(?: ["'](.+)["'])? should be the following json$/) do |response_key, expected_json_text|
  response_hash = @app_helper.response
  response_hash = @app_helper.response.dig(*response_key.split('.').map(&:to_sym)) unless response_key.blank?
  expected_hash = JSON.parse(expected_json_text.strip)

  expected_json = JSON.generate(expected_hash)
  response_json = JSON.generate(response_hash)

  expect(response_json).to eq(expected_json)
end

Then(/^the server response(?: ["'](.+)["'])? should contains the following json[:]?$/) do |response_key, expected_json_text|
  response_hash = @app_helper.response
  response_hash = @app_helper.response.dig(*response_key.split('.').map(&:to_sym)) unless response_key.blank?
  expected_hash = JSON.parse(expected_json_text.strip, symbolize_names: true)

  expected_hash = response_hash.deep_merge(expected_hash) do |key, response_value, expected_value|
    next expected_value if response_value.class != Array

    expect(response_value.size).to eq(expected_value.size), "Array '#{key}' doesn't have same size.\nexpected: #{expected_value.size}\n     got: #{response_value.size}\n"

    response_value.map.with_index do |_, i|
      response_value[i].deep_merge(expected_value[i])
    end
  end

  expected_json = JSON.generate(expected_hash)
  response_json = JSON.generate(response_hash)

  expect(response_json).to eq(expected_json)
end

###
# It validates a model field. Example:
#
# Given the following "Player"
#   | id                                   | name |
#   | 00000000-0000-0000-0000-000000000001 | Jose |
# Then the "name" of the "Player" with "id" "00000000-0000-0000-0000-000000000001" should be "Jose"
Then(/^the ["'](.+)["'] of the ["'](.+)["'] with ["'](.+)["'] ["'](.+)["'] should be ["'](.+)["']$/) do |model_attribute, model_name, search_attribute_name, search_attribute_value, expected_value|
  current_value = model_name.constantize.where(search_attribute_name => search_attribute_value).select(search_attribute_name).limit(1).pluck(model_attribute).first

  current_value = to_string(current_value)
  expected_value = to_string(expected_value)

  expect(current_value).to eq(expected_value)
end

Then(/^the ["'](.+)["'] of the ["'](.+)["'] with ["'](.+)["'] ["'](.+)["'] should not be ["'](.+)["']$/) do |model_attribute, model_name, search_attribute_name, search_attribute_value, expected_value|
  current_value = model_name.constantize.where(search_attribute_name => search_attribute_value).select(search_attribute_name).limit(1).pluck(model_attribute).first

  current_value = to_string(current_value)
  expected_value = to_string(expected_value)

  expect(current_value).not_to eq(expected_value)
end


###
# It count the number of models. Example
#
# Then the number of 'Users' should be 10
Then(/^the number of ["'](.+)["'] should be (\d+)$/) do |model_name, model_amount|
  model_name = model_name.singularize

  expected_value = model_amount
  current_value = model_name.constantize.count

  expect(current_value).to eq(expected_value)
end

Given(/^print server response$/) do
  puts JSON.pretty_generate(@app_helper.response)
end

Before do
  ActiveRecord::Base.descendants.each { |c| c.delete_all unless c == ActiveRecord::SchemaMigration }
  RubyPitaya::AppSpecHelper.update_before_each
  @app_helper = RubyPitaya::AppSpecHelperClass.new('cucumber')
end

# After do
# end