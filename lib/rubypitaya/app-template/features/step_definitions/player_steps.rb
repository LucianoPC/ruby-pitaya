Given(/^the following Player[s]?$/) do |table|
  player_hashes = table.hashes
  player_hashes.each do |player_hash|
    player_hash[:user] = User.new(id: player_hash[:user_id])
    Player.create(player_hash)
  end
end

Given(/^the Player ["'](.+)["'] is authenticated$/) do |player_name|
  player = Player.find_by_name(player_name)
  @app_helper.authenticate(player.user_id)
end
