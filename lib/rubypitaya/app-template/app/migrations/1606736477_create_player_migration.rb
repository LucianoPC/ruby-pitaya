require 'active_record'

class CreatePlayerMigration < ActiveRecord::Migration[7.0]

  def change
    create_table :players do |t|
      t.belongs_to :user, foreing_key: true, index: { unique: true }
      t.string :name, null: false
      t.integer :gold, null: false
      t.timestamps null: false
    end
  end
end
