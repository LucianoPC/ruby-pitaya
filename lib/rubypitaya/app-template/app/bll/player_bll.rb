class PlayerBLL

  def create_new_player(setup, config)
    name = config['initial_player'][:name]
    gold = config['initial_player'][:wallet][:gold]

    player = Player.new(name: name, gold: gold, user: User.new)
    player.save!
    player
  end
end