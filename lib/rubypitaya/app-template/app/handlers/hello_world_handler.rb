class HelloWorldHandler < RubyPitaya::HandlerBase

  non_authenticated_actions :sayHello, :showMessage

  def sayHello
    response = {
      code: StatusCodes::CODE_OK,
      data: {
        message: 'Hello!'
      }
    }
  end

  def showMessage
    message = @params[:message]

    response = {
      code: StatusCodes::CODE_OK,
      data: {
        message: message
      }
    }
  end
end