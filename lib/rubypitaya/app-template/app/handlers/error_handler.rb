class ErrorHandler < RubyPitaya::HandlerBase

  non_authenticated_actions :getError

  def getError
    error_code = RubyPitaya::StatusCodes::CODE_ERROR
    error_message = "Some Error"

    raise RubyPitaya::RouteError.new(error_code, error_message)

    response = {
      code: StatusCodes::CODE_OK,
      data: {
        message: 'Ok!'
      }
    }
  end
end