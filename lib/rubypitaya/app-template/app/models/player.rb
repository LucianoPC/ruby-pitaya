require 'active_record'

class Player < ActiveRecord::Base

  belongs_to :user

  validates_presence_of :name, :gold, :user

  def to_hash
    {
      name: name,
      gold: gold,
      userId: user_id,
    }
  end
end