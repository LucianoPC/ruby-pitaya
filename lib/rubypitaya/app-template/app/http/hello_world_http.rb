require 'rubypitaya/core/http_routes'

RubyPitaya::HttpRoutes.class_eval do

    get '/hello-world/html' do
      content_type 'text/html'

      @message = "Hello World"

      erb :hello_world
    end

    get '/hello-world/json' do
      response = {
        message: 'Hello World!'
      }.to_json
    end

  private

  def response_error(message)
    response = {
      code: StatusCodes::CODE_ERROR,
      message: message,
    }.to_json
  end
end