require 'spec_helper'

RSpec.describe 'RubyPitaya::RedisService', type: :request do
  it 'mock service' do
    allow(services[:redis]).to receive(:get).with('key').and_return('something')

    value = services[:redis].get('key')

    expect(value).to eq('something')
  end

  it 'not mock service' do
    services[:redis].set('foo', 'bar')

    value = services[:redis].get('foo')

    expect(value).to eq('bar')
  end
end