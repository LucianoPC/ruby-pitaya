require 'spec_helper'

RSpec.describe 'HelloWorldHandler', type: :request do
  context 'sayHello' do
    it 'success' do
      request("rubypitaya.helloWorldHandler.sayHello")

      expected_response = {
        code: 'RP-200',
        data: { message: 'Hello!' } 
      }

      expect(response).to eq(expected_response)
    end
  end
end