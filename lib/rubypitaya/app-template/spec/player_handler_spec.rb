require 'spec_helper'

RSpec.describe 'PlayerHandler', type: :request do
  context 'authenticate' do
    it 'create_new_user' do
      set_config({
        'initial_player' => {
          'name' => 'Guest',
          'wallet' => {'gold' => 10},
        }
      })

      params = {}
      request("rubypitaya.playerHandler.authenticate", params)

      player = Player.last

      expect(response[:code]).to eq('RP-200')
      expect(response[:data][:name]).to eq('Guest')
      expect(response[:data][:gold]).to eq(10)

      expect(User.count).to eq(1)
      expect(Player.count).to eq(1)
      expect(player.name).to eq('Guest')
      expect(player.gold).to eq(10)
    end
  end

  context 'getInfo' do
    it 'success' do
      player = Player.create(name: 'Someone', gold: 12, user: User.new)

      authenticate(player.user_id)

      request("rubypitaya.playerHandler.getInfo")

      expect(response[:code]).to eq('RP-200')
      expect(response[:data]).to eq(player.to_hash)
    end

    it 'error_not_authenticated' do
      request("rubypitaya.playerHandler.getInfo")

      expect(response[:code]).to eq(RubyPitaya::StatusCodes::CODE_NOT_AUTHENTICATED)
      expect(response[:message]).to eq('Not authenticated')
    end
  end
end