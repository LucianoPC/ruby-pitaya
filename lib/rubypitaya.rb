require 'erb'
require 'ostruct'
require 'fileutils'

require 'rubypitaya/core/main'

module RubyPitaya

  class RubyPitaya

    def self.run_server
      Main.new
    end

    def self.create_project(project_name, folder_path)
      project_path = File.join(folder_path, project_name)
      app_template_path = Path::APP_TEMPLATE_FOLDER_PATH

      FileUtils.cp_r app_template_path, project_path
    end

    def self.create_migration(migration_name)
      migration_name = "#{migration_name}_migration" unless migration_name.underscore.end_with?('migration')
      migration_timestamp = Time.now.utc.to_i
      migration_file_name = "#{migration_timestamp}_#{migration_name.underscore}.rb"
      migration_class_name = migration_name.camelcase

      template_struct = OpenStruct.new(
        class_name: migration_class_name,
      )

      template = File.open(Path::MIGRATION_TEMPLATE_PATH, &:read)
      template_result = ERB.new(template).result(template_struct.instance_eval { binding })

      migration_file_path = File.join(Path::MIGRATIONS_FOLDER_PATH, migration_file_name)
      File.open(migration_file_path, 'w') { |f| f.write(template_result) }

      migration_file_name
    end

    def self.add_plugin(plugin_git_url, branch_name)
      Dir.mkdir(Path::PLUGINS_FOLDER_PATH) unless File.exists?(Path::PLUGINS_FOLDER_PATH)

      plugin_name = plugin_git_url.scan(/.+\/(.+)\.git/).flatten.first
      plugin_folder_path = File.join(Path::PLUGINS_FOLDER_PATH, plugin_name)
      plugin_git_path = File.join(plugin_folder_path, '.git/')

      commit_hash = ''
      branch_command = ''
      branch_command = "--branch #{branch_name}" unless branch_name.blank?

      FileUtils.rm_rf(plugin_folder_path) if File.exists?(plugin_folder_path)
      puts "git -C #{Path::PLUGINS_FOLDER_PATH} clone  --depth 1 #{branch_command} #{plugin_git_url}"
      `git -C #{Path::PLUGINS_FOLDER_PATH} clone --depth 1 #{branch_command} #{plugin_git_url}`
      commit_hash = `git rev-parse --short HEAD`.strip
      FileUtils.rm_rf(plugin_git_path)

      Dir.entries(plugin_folder_path).each do |entry|
        entry_path = File.join(plugin_folder_path, entry)
        FileUtils.rm_rf(entry_path) unless entry == 'app'      ||
                                           entry == 'spec'     ||
                                           entry == 'features' ||
                                           entry == '.'        ||
                                           entry == '..'
      end

      plugin_migrations_path = File.join(plugin_folder_path, 'app/migrations/')
      plugin_migrations_files = Dir[File.join(plugin_migrations_path, '*')]
      base_migration_timestamp = Time.now.utc.to_i

      plugin_migrations_files.each_with_index do |migration_file, i|
        migration_timestamp = base_migration_timestamp + i
        new_file = migration_file.gsub(/^(.+\/migrations\/)\d+(_.+)$/, "\\1#{migration_timestamp}\\2")

        File.rename(migration_file, new_file)
      end

      plugins_config = YAML.load(File.open(Path::PLUGIN_CONFIG_FILE_PATH, &:read))
      plugins_config['plugins'][plugin_name] = {
        'git' => plugin_git_url,
        'branch' => branch_name || '',
        'commit' => commit_hash,
      }

      File.open(Path::PLUGIN_CONFIG_FILE_PATH, 'w') { |f| f.write(plugins_config.to_yaml) }

      plugin_name
    end
  end
end
