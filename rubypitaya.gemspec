lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rubypitaya/version'

Gem::Specification.new do |spec|
  spec.name          = 'rubypitaya'
  spec.version       = RubyPitaya::VERSION
  spec.authors       = ['Luciano Prestes Cavalcanti']
  spec.email         = ['lucianopcbr@gmail.com']

  spec.summary       = 'Create servers with pitaya'
  spec.description   = 'RubyPitaya is an application to create servers using ' \
                       'the pitaya protocol.'
  spec.homepage      = 'https://gitlab.com/LucianoPC/ruby-pitaya'
  spec.license       = 'MIT'

  spec.files         = Dir['./lib/**/*',
                           './lib/rubypitaya/app-template/.gitignore',
                           './lib/rubypitaya/app-template/.gitlab-ci.yml',
                           './lib/rubypitaya/app-template/.irbrc']

  spec.bindir        = 'bin'
  spec.executables  << 'rubypitaya'
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'pg', '1.5.6'
  spec.add_runtime_dependency 'rake', '13.2.1'
  spec.add_runtime_dependency 'redis', '5.2.0'
  spec.add_runtime_dependency 'etcdv3', '0.11.6'
  spec.add_runtime_dependency 'ostruct', '0.6.0'
  spec.add_runtime_dependency 'sinatra', '3.2.0'
  spec.add_runtime_dependency 'webrick', '1.8.1'
  spec.add_runtime_dependency 'nats-pure', '2.4.0'
  spec.add_runtime_dependency 'activerecord', '7.1.3.3'
  spec.add_runtime_dependency 'google-protobuf', '3.25.2'

  spec.add_development_dependency 'pry', '0.14.2'
  spec.add_development_dependency 'rspec', '3.13.0'
  spec.add_development_dependency 'listen', '3.9.0'
  spec.add_development_dependency 'bundler', '2.4.19'
  spec.add_development_dependency 'cucumber', '9.2.0'
  spec.add_development_dependency 'sinatra-contrib', '3.2.0'
end