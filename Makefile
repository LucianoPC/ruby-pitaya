## + Server Commands

## Run ruby pitaya metagame project
run:
	@docker-compose run --service-ports --rm rubypitaya bundle exec ruby ../../../bin/rubypitaya run

## Build project docker containers
build:
	@docker-compose build
	@docker-compose pull
#	@echo "Copying vendor folder ..."
#	@rm -rf ./vendor
#	@sh -c "FOLDER=$(notdir $(shell pwd)); docker cp \$$(docker-compose run --no-deps --detach --name=rubypitaya rubypitaya):/app/rubypitaya/vendor ."
#	@docker rm -f rubypitaya

## Setup project dependencies
setup: clear
	@docker-compose run --service-ports --rm rubypitaya ./setup/development/main

## Run ruby irb console
console:
	@docker-compose run --service-ports --rm rubypitaya-console bundle exec ruby ../../../bin/console

## Run bash on container
bash:
	@docker-compose run --service-ports --rm rubypitaya bash

## + Test Commands

## Run all tests
test: test-rspec test-cucumber

## Run rspec tests. FILE=spec.rb:10
test-rspec:
	@docker-compose run --service-ports --rm -e RUBYPITAYA_SERVER_ENVIRONMENT="test" rubypitaya bundle exec rspec $(FILE)

## Run cucumber tests. FILE=something.feature
test-cucumber:
	@docker-compose run --service-ports --rm -e RUBYPITAYA_SERVER_ENVIRONMENT="test" rubypitaya bundle exec cucumber --publish-quiet $(FILE)

## Setup project dependencies for tests
setup-test: clear
	@docker-compose run --service-ports --rm -e RUBYPITAYA_SERVER_ENVIRONMENT="test" rubypitaya ./setup/test/main

## + Improve metagame

## Create new migrgation. NAME=[migration-name]
create-migration:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec ruby ../../../bin/rubypitaya create-migration $(NAME)

## Add or update a plugin. GIT=[plugin-http-git-link] BRANCH=[branch-name:optional]
add-plugin:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec ruby ../../../bin/rubypitaya add-plugin $(GIT) $(BRANCH)

## + Database Commands

## Create database
db-create:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:create

## Run migrations to database
db-migrate:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:migrate

## Show migrations status on database
db-migrate-status:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:status

## Rollback migrations. STEP=0
db-rollback:
	@docker-compose run --service-ports --rm -e STEP="$(STEP)" rubypitaya-commands bundle exec rake db:rollback

## Drop database
db-drop:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:drop

## Reset database
db-reset:
	@docker-compose run --service-ports --rm rubypitaya-commands bundle exec rake db:reset

## Setup test database
db-test-setup:
	@docker-compose run --service-ports --rm -e RUBYPITAYA_SERVER_ENVIRONMENT="test" rubypitaya-commands bundle exec rake db:test:setup

## + Other Commands

## Generate protobuf files. TODO: Dockerize it
protos:
	@protoc --proto_path=./lib/rubypitaya/core/protos/ --ruby_out=./lib/rubypitaya/core/protos/ ./lib/rubypitaya/core/protos/*.proto

## Clear docker containers and volumes
clear:
	@docker rm -f $$(docker ps -aq) ; docker volume rm -f $$(docker volume ls)

.DEFAULT_GOAL := show-help

.PHONY: show-help
show-help:
	@echo "$$(tput bold)Commands:$$(tput sgr0)"
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		if (length($$1) == 0) { \
		    printf "\n"; \
		} \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}'
